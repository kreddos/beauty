/**
 * Created by dmitry on 05.09.16.
 */
angular.module('app').service('defaultService', function () {
    this.openModal = openModal;

    function openModal(modalId) {
        $('#' + modalId).modal('show');
    }
});