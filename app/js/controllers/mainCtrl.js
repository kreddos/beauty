/**
 * Created by dmitry on 05.09.16.
 */
angular.module('app').controller('MainCtrl', MainCtrl);
MainCtrl.$inject = ['$scope', 'defaultService'];

function MainCtrl($scope, defaultService){
    var vm = this;
    vm.openModal = defaultService.openModal;
}