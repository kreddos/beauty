angular.module('app').controller('AboutCtrl', AboutCtrl);
AboutCtrl.$inject = ['$scope', 'defaultService'];

function AboutCtrl($scope, defaultService){
	var vm = this;
	vm.openModal = defaultService.openModal;
	vm.opentAbonementModal = opentAbonementModal;
	vm.openProcedureModal = openProcedureModal;

	function opentAbonementModal(type) {
		vm.typeModal = type;
		vm.openModal('abonement_modal');
	}
	function openProcedureModal(type) {
		vm.typeProcedureModal = type;
		vm.openModal('procedure_modal');
	}
}