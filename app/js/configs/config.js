/**
 * Created by dmitry on 01.09.16.
 */
angular.module('app').config(['$stateProvider','$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    //
    // For any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise("/about");
    //
    // Now set up the states
    $stateProvider
        .state('app', {
            templateUrl: "/app/html/controllers_templates/app.html",
            controller: "MainCtrl",
            controllerAs: "vm"
        })
        .state('app.about', {
            url: "/about",
            templateUrl: "/app/html/controllers_templates/about.html",
            controller: "AboutCtrl",
            controllerAs: "vm"
        })
}]);