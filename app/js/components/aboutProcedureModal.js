/**
 * Created by dmitry on 05.09.16.
 */
angular.module('app').component('aboutProcedureModal', {
    templateUrl: "/app/html/components/about_procedure_modal.html",
    controller: CallbackModal,
    controllerAs: 'vm'
});
function CallbackModal($scope, defaultService) {
    var vm = this;
    vm.openModal = defaultService.openModal;
}