/**
 * Created by dmitry on 05.09.16.
 */
angular.module('app').component('abonementModal', {
    templateUrl: "/app/html/components/abonement_modal.html",
    controller: abonementModal,
    controllerAs: 'vm',
    bindings: {
        type: '=',
    }
});
function abonementModal($scope, defaultService) {
    var vm = this;
    vm.openModal = defaultService.openModal;
}