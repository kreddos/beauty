/**
 * Created by dmitry on 05.09.16.
 */
angular.module('app').component('callbackModal', {
    templateUrl: "/app/html/components/modal_callback.html",
    controller: CallbackModal,
    controllerAs: 'vm'
});
function CallbackModal($scope, defaultService) {
    var vm = this;
    vm.openModal = defaultService.openModal;
}